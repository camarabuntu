#! /bin/bash

./bin/setup-kqemu.sh

if [[ ! -f hda.qcow ]] ; then
    echo "Creating the harddrive image file"
    qemu-img create -f qcow2 hda.qcow 10G
fi
sudo qemu -cdrom $* -hda hda.qcow -m 256 -boot d -monitor stdio
