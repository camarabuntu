#! /bin/bash


# check if the ubuntu 6.06 ISO is there
if [[ ! -f ./isos/ubuntu-6.06.1-alternate-i386.iso ]] ; then
    echo "You need to download the base ISO first"
    exit 1
fi

echo "Extracting the files from the CD"
mkdir -p dapper
./bin/extract-cd-contents.sh isos/ubuntu-6.06.1-alternate-i386.iso dapper

echo "Copying files across"
zcat ./isolinux/splash.pcx.gz > ./dapper/isolinux/splash.pcx
cp ./isolinux/isolinux-6.06.cfg ./dapper/isolinux/isolinux.cfg
cp ./preseed/camarabuntu-cd-install-6.06-brazzaville.seed ./dapper/preseed/camarabuntu-cd-install.seed

echo "Creating the ISO in isos/camarabuntu-6.06-addis-ababa.iso..."
./bin/make-cd-iso.py -d dapper -n "Camarabuntu 6.06 Addis Ababa" -f isos/camarabuntu-6.06-addis-ababa.iso

echo "Done. You can test the ISO with this command"
echo "sudo qemu -cdrom isos/camarabuntu-6.06-addis-ababa.iso -hda hda.qcow -m 256 -boot d -monitor stdio"
