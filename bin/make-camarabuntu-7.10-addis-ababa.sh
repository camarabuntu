#! /bin/bash


# check if the ubuntu 7.10 ISO is there
if [[ ! -f ./isos/ubuntu-7.10-alternate-i386.iso ]] ; then
    echo "Downloading the ISO"
    ./bin/dl-ubuntu-7.10.sh
fi

mkdir -p gutsy
./bin/extract-cd-contents.sh isos/ubuntu-7.10-alternate-i386.iso gutsy

echo "Copying various files across..."
cp ./preseed/camarabuntu-cd-install-8.04-addis-ababa.seed ./gutsy/preseed/camarabuntu-cd-install.seed
zcat ./isolinux/splash.pcx.gz > ./gutsy/isolinux/splash.pcx
cp ./isolinux/isolinux-8.04.cfg ./gutsy/isolinux/isolinux.cfg
cp ./isolinux/lang ./gutsy/isolinux/

echo "Creating the ISO in isos/camarabuntu-7.10-addis-ababa.iso..."
./bin/make-cd-iso.py -d gutsy -n "Camarabuntu 7.10 Addis Ababa" -f isos/camarabuntu-7.10-addis-ababa.iso

echo "Done. You can test the ISO with this command"
echo "sudo qemu -cdrom isos/camarabuntu-7.10-addis-ababa.iso -hda hda.qcow -m 256 -boot d -monitor stdio"
