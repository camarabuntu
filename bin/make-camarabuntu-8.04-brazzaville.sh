#! /bin/bash


# check if the ubuntu 8.04 ISO is there
if [[ ! -f ./isos/ubuntu-8.04.1-alternate-i386.iso ]] ; then
    echo "You need to download the base ISO first"
    exit 1
fi

mkdir -p hardy
./bin/extract-cd-contents.sh isos/ubuntu-8.04.1-alternate-i386.iso hardy

echo "Copying various files across..."
cp ./preseed/camarabuntu-cd-install-8.04-brazzaville.seed ./hardy/preseed/camarabuntu-cd-install.seed
zcat ./isolinux/splash.pcx.gz > ./hardy/isolinux/splash.pcx
cp ./isolinux/isolinux-8.04.cfg ./hardy/isolinux/isolinux.cfg
cp ./isolinux/lang ./hardy/isolinux/

echo "Creating the ISO in isos/camarabuntu-8.04-brazzaville.iso..."
./bin/make-cd-iso.py -d hardy -n "Camarabuntu 8.04 Brazzaville" -f isos/camarabuntu-8.04-brazzaville.iso

echo "Done. You can test the ISO with this command"
echo "sudo qemu -cdrom isos/camarabuntu-8.04-brazzaville.iso -hda hda.qcow -m 256 -boot d -monitor stdio"
